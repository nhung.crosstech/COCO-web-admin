import { AccountService } from '@/services/Account';

const formatData = (data) => {
  for (let i = 0; i < data.length; i++) {
    data[i].key = i;
  }
  return data;
};
const getdata = async (role, page, limit, setdata) => {
  try {
    const response = await AccountService.getUser(role, page, limit);
    if (response?.success) {
      setdata(formatData(response.users));
    }
  } catch (err) {
    console.log('Error is:', err);
    // setLoading(false);
  }
};
export const getdataMentorPanding = async (page, limit, setdata) => {
  try {
    const response = await AccountService.mentorPending(page, limit);
    if (response?.success) {
      setdata(response.data);
    }
  } catch (err) {
    console.log('Error is:', err);
    // setLoading(false);
  }
};

export default getdata;
