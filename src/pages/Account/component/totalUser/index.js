import { AccountService } from '@/services/Account';
const gettotalUserRole = async (role, setTotal) => {
  try {
    const response = await AccountService.totalUserRole(role);
    if (response?.success) {
      setTotal(response.count);
    }
  } catch (err) {
    console.log('Error is:', err);
  }
};

export default gettotalUserRole;
