// import Table from '../../component/table';

import { useEffect, useState } from 'react';

import gettotalUserRole from '../component/totalUser';
import getdata from '../component/dataUser';
import { Table, Pagination } from 'antd';
import { columns } from './columns';
const Mentee = () => {
  const [data, setdata] = useState([]);
  const [total, setTotal] = useState(1);

  useEffect(() => {
    gettotalUserRole('5', setTotal);
    getdata(5, 1, 10, setdata);
  }, []);

  return (
    <>
      <h1>mentee</h1>
      <Table
        columns={columns(setdata)}
        dataSource={data}
        pagination={false}
      ></Table>
      <Pagination
        total={total}
        showTotal={(total, range) => {
          return `${range[0]}-${range[1]} of ${total} items`;
        }}
        defaultPageSize={10}
        defaultCurrent={1}
        onChange={(page, pageSize) => {
          getdata(5, page, pageSize, setdata);
        }}
      />
    </>
  );
};
export default Mentee;
