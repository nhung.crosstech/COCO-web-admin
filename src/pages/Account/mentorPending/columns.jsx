import { CloseOutlined, EyeOutlined, CheckOutlined } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';
import routerLinks from '@/utils/router-links';
import { showApproveModal } from '@/components/AccountModal/Modal';
import confirmMentor from '../component/confirmMentor';
import { Divider } from 'antd';
export const columns = (setData, data) => {
  const navigate = useNavigate();
  return [
    {
      title: 'HỌ TÊN',
      key: '1',
      dataIndex: 'fullname',
      render: (_, info) => <>{info.user?.fullname}</>,
    },

    {
      title: 'EMAIL',
      key: '2',
      dataIndex: 'email',
      render: (_, info) => <>{info.user?.email}</>,
    },
    {
      title: 'SỐ ĐIỆN THOẠI',
      key: '3',
      dataIndex: 'phone',
      render: (_, info) => <>{info.user?.phone}</>,
    },
    {
      title: 'LĨNH VỰC',
      key: '4',
      dataIndex: 'field',
      render: (_, info) => <>{info.user?.field}</>,
    },
    {
      title: 'NGÀY HOẠT ĐỘNG',
      key: '5',
      dataIndex: 'activeDay',
      render: (_, info) => <>{info.user?.activeDay}</>,
    },
    {
      title: 'TRẠNG THÁI',
      key: 'action',
      render: (payload) => {
        return <div>{payload.is_active ? 'Đang hoạt động' : 'Vô hiệu'}</div>;
      },
    },
    {
      title: 'HOẠT ĐỘNG',
      key: '6',
      render: (_, info) => (
        <>
          <EyeOutlined
            onClick={() => {
              navigate(
                `${info.user.id}`,
                { state: info.registration },
                { replace: true }
              );
            }}
          />
          <Divider type="vertical" />
          <CheckOutlined
            onClick={() => {
              showApproveModal(true, () => {
                confirmMentor(info.registration.id, 'approve', setData, data);
              });
            }}
          />
          <Divider type="vertical" />
          <CloseOutlined
            style={{ color: 'red' }}
            onClick={() => {
              showApproveModal(false, () => {
                confirmMentor(info.registration.id, 'reject', setData, data);
              });
            }}
          />
        </>
        // </Space>
      ),
    },
  ];
};
