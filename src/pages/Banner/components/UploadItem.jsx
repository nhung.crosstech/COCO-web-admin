import {
  showConfirmError,
  showConfirmSuccess,
  showDeleteBanner,
} from '@/components/AccountModal/Modal';
import { BannerService } from '@/services/banner';
import { DeleteOutlined, PictureOutlined } from '@ant-design/icons';
import { Button, Form, Select, Typography } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { useEffect, useRef, useState } from 'react';

const { Title } = Typography;

const UploadItem = ({
  keyBanner,
  initBanners,
  linkCategory,
  linkMentor,
  linkCourse,
  handleChangeLinks,
}) => {
  const [image, setImage] = useState({ file: null, isUploaded: false });
  const [preview, setPreview] = useState();
  const [link, setLink] = useState();

  const [form] = useForm();
  const uploadRef = useRef(null);

  const handleClickUpload = () => {
    uploadRef.current.click();
  };

  const handleUploadImages = (e) => {
    const imageFile = e.target.files[0];
    setImage({ file: imageFile, isUploaded: false });
    e.target.value = null;
    const objectUrl = URL.createObjectURL(imageFile);
    setPreview(objectUrl);
  };

  useEffect(() => {
    if (initBanners[keyBanner]?.banner_url) {
      setImage((pre) => ({ ...pre, isUploaded: true }));
      setPreview(
        import.meta.env.VITE_REACT_APP_API_URL +
          initBanners[keyBanner].banner_url
      );
      form.resetFields();
    }
  }, [JSON.stringify(initBanners[keyBanner])]);

  useEffect(() => {
    if (linkCourse && linkMentor && initBanners[keyBanner]?.target) {
      setLink(
        initBanners[keyBanner]?.target === 'Course' ? linkCourse : linkMentor
      );
    }
  }, [JSON.stringify(linkCourse), JSON.stringify(linkMentor)]);

  useEffect(() => {
    const data = link?.find(
      (link) => link.key === initBanners[keyBanner]?.target_id
    );
    form.setFieldValue('target_id', data?.value);
  }, [JSON.stringify(link)]);

  const onSubmit = async (values) => {
    try {
      const form = new FormData();
      form.append(`banner`, image.file);
      const query = `&target=${values.target}&target_id=${values.target_id.key}`;
      const res = await BannerService.uploadBanners(keyBanner, form, query);
      if (res?.success) {
        setImage((pre) => ({ ...pre, isUploaded: true }));
        showConfirmSuccess();
      }
    } catch (error) {
      showConfirmError();
      throw error;
    }
  };

  const onDelete = async () => {
    const doDelete = () => {
      URL.revokeObjectURL(preview);
      setPreview();
      setImage({ file: null, isUploaded: false });
      form.setFieldsValue({ target: null, target_id: null });
    };
    if (!image.isUploaded) {
      doDelete();
    } else {
      showDeleteBanner(keyBanner, async () => {
        const res = await BannerService.deleteBanner(keyBanner);
        if (res?.success) {
          doDelete();
          showConfirmSuccess();
        } else {
          showConfirmError();
        }
      });
    }
  };

  const onChangeOptions = async (type) => {
    switch (type) {
      case 'Mentor':
        setLink(linkMentor);
        break;
      case 'Course':
        setLink(linkCourse);
        break;
      default:
        break;
    }
  };

  return (
    <Form
      onFinish={onSubmit}
      form={form}
      initialValues={{ target: initBanners[keyBanner]?.target }}
    >
      <div className="flex flex-col justify-center">
        <Title level={4}>{keyBanner}</Title>
        <Form.Item>
          <div
            className={`flex flex-col justify-center w-full h-[256px] px-4 py-6 cursor-pointer relative ${
              !preview
                ? `rounded-lg border-dashed border-2 border-[#475569]`
                : ''
            }`}
          >
            <input
              className="invisible"
              ref={uploadRef}
              type="file"
              onChange={handleUploadImages}
              accept=".jpg, .png"
            />
            {!preview ? (
              <section
                className="flex flex-col items-center justify-center h-full gap-4 text-center"
                onClick={handleClickUpload}
              >
                <PictureOutlined className="text-2xl" />
                <span>
                  Kéo thả ảnh vào khung hình hoặc nhấn tải lên từ thiết bị
                </span>
              </section>
            ) : (
              <div>
                <img
                  className="object-fill w-full h-full absolute top-0 left-0"
                  src={preview}
                />
                <div className="absolute top-0 left-0 opacity-0 w-full h-full bg-[#00000080] duration-300 hover:opacity-100">
                  <DeleteOutlined
                    className="absolute top-4 right-4 text-[18px] text-white"
                    onClick={onDelete}
                  />
                </div>
              </div>
            )}
          </div>
        </Form.Item>
        <p className="text-center">
          {'Chỉ định dạng *.PNG, *.JPG và *.JPEG. (Tối đa 2MB px)'}
        </p>
        <div>
          <Title level={5}>Liên kết tới</Title>
          <section className="flex">
            <Form.Item name="target" className="flex-none w-1/3">
              <Select
                // defaultValue={initBanners[keyBanner]?.target}
                options={linkCategory}
                placeholder="Mục Liên Kết"
                onChange={onChangeOptions}
              />
            </Form.Item>
            <Form.Item name="target_id" className="grow ml-6">
              <Select
                // defaultValue={selectedLink}
                labelInValue
                placeholder="Chọn Liên Kết"
                options={link}
              />
            </Form.Item>
          </section>
        </div>
        <div className="self-end">
          <Button type="primary" htmlType="submit">
            Lưu
          </Button>
        </div>
      </div>
    </Form>
  );
};

export default UploadItem;
