import { HomeOutlined } from '@ant-design/icons';
import { Books, BellRing, Stack, User } from '@/assets';
const Layout = [
  {
    label: 'Dashboard',
    key: 'Dashboard',
    icon: <HomeOutlined />,
  },
  {
    label: 'Quản lý khóa học',
    key: 'Quản lý khóa học',
    icon: <img src={Books} />,
  },
  {
    label: 'Quản lý tài khoản',
    key: 'QUẢN LÝ TÀI KHOẢN',
    icon: <img src={User} />,

    children: [
      {
        label: 'Tài khoản người dùng',
        key: 'TÀI KHOẢN NGƯỜI DÙNG',
        // icon: <HomeOutlined />,
      },
      {
        label: 'Tài khoản hệ thống',
        key: 'TÀI KHOẢN HỆ THỐNG',
        // icon: <HomeOutlined />,
      },
      {
        label: 'Vai trò',
        key: 'VAI TRÒ',
        // icon: <HomeOutlined />,
      },
    ],
  },
  {
    label: 'Quản lý thông báo',
    key: 'notification',
    icon: <img src={BellRing} />,
  },
  {
    label: 'Quản lý thông báo',
    key: 'QUẢN LÝ TIỆN ÍCH',
    icon: <img src={Stack} />,

    children: [
      {
        label: 'Banner',
        key: 'banner',
        // icon: <HomeOutlined />,
      },
      {
        label: 'Tuyển dụng',
        key: 'TUYỂN DỤNG',
        // icon: <HomeOutlined />,
      },
      {
        label: 'Blog',
        key: 'BLOG',
        // icon: <HomeOutlined />,
      },
      {
        label: 'Cv',
        key: 'CV',
        // icon: <HomeOutlined />,
      },
    ],
  },
];
export default Layout;
