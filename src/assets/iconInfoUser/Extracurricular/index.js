import ChalkboardTeacher from '../Icon-ChalkboardTeacher.svg';
import UsersThree from './Icon-UsersThree.svg';
import Note from '../Icon-Note.svg';
import Clock from '../Icon-Clock.svg';
export { ChalkboardTeacher, Note, Clock, UsersThree };
