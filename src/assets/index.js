import Books from '@/assets/Books.svg';
import BellRing from '@/assets/Icon-BellRinging.svg';
import Stack from '@/assets/Icon-Stack.svg';
import User from '@/assets/UserCircleGear.svg';
import Logo from '@/assets/logo1.svg';
import ArrowSquareLeft from '@/assets/Icon-ArrowSquareLeft.svg';
export { Books, BellRing, Stack, User, Logo, ArrowSquareLeft };
