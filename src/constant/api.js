export const USER_API_PATH = 'users';
export const BANNER_API_PATH = 'banners';
export const NOTIFICATION_API_PATH = 'notifications';
export const COURSE_API_PATH = 'courses';
